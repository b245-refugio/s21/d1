// console.log("asdsd");

// [Array]
	// An array in programming is simply a list of data. Data that are related/connected with each other.

let studentA = "2020-1923";
let studentB = "2020-1924";
let studentC = "2020-1925";
let studentD = "2020-1926";
let studentE = "2020-1927";
	
	// now, with array, we can simply write the code above like this:

	let studentNumbers = ["2020-1923","2020-1924","2020-1925","2020-1926","2020-1927"];

	// Arrays are used to store multiple related values in a single variable.
	// they are declared using square brackets also know as the "Array literals"
	// Commonly used to store numerous amounts of data to manipulate in order to perform a number of task.
	// array also provide acccess of functions/methods that help achieving specific
	// a method is another term for functions associated with an object/array an is used to execute statements that are relevant

	// Property and Method
		// .length - property
		// .toLowerCasse(); - method

	let object = {
		name: "Jimmy",
		age: 25,
		location: "Makati"
	}
	console.log(object.name);

	// Array is also object which is another type.
	console.log(typeof studentNumbers);

	/*
		Syntax:
			let/const arrayName = [element0,element1,....]

	*/

	// common sxample of arrays
	const grades = [98.5, 94.3, 89.2, 90.1];
	console.log(grades);
	grades[1] = 80;
	// grade = [];

	// another example
	let computerBrands = ["Acer","Asus","Lenovo","Dell","Mac","Samsung"];
	console.log(computerBrands);

	// Possible use of an array
	let mixedArr = [12,'Asus', null,undefined,{}];
	console.log(mixedArr);

	// Create an array with values from variables

	let city1 = "Tokyo";
	let city2 = "Manila";
	let city3 = "New York";

	// if we use variable as element of our array the value will be passed.
	let cities = [city1,city2,city3];
	console.log(cities);

	// [SECTION] Length Property
		// The .length property allows us to get and set the total number of items.elements in an array.
	console.log(grades.length);
	console.log(typeof grades.length);

	let blankArr = [];
	console.log(blankArr.length);

	let array;
	console.log(array);

	// .length property can also be used with strings. some array methods and properties can also be used.

	let fullName = "Renz Mae";
	console.log(fullName.length);
	// we cant change the length value of a string
	fullName.length = fullName.length - 1;
	console.log(fullName);
	// length property on strings shows the numbers of characters in a string. spaces are counted as charaters in Strings.

	// length property can also set the number of the items in an array, meaning we can actually delete the last item in the array or shorten the array by simply updating the length property of an array

	let myTask = [
			"dtink HTML",
			"eat JavaScript",
			"inhale CSS",
			"bake Sass"
		];
	console.log(myTask);
	myTask.length = myTask.length-1;
	console.log(myTask);

	// function removeElement(array, index){
	// 	let secondArray = array;
	// 	let remove = array.length - index;
	// 	array.length = array.length - remove
	// 	console.log(array);

	// 	for(let i = index+1; i <= secondArray.length-1; i++ ){
	// 		array+=secondArray[i];
	// 	}
	// 	console.log(array);
	// }
	// removeElement(myTask,2);

	// To delete a specific item in an array we can employ or use array methods.



	// if you can shorten the array by setting the length of property, you can also lengthen it by adding a numbeer into length property

	let theBeatles = ["John","Paul","Ringo","George"];

	console.log(theBeatles);
	theBeatles[theBeatles.length] = "Chris";
	console.log(theBeatles);
	theBeatles[theBeatles.length] = "Jimmy";
	console.log(theBeatles);

// [SECTION] Reading/Accessing elements of arrays
	// accessing array elements is one of the more common task that we do in array
	// this can be done through the use of its index
	// Each element in an array is associated with its index/number

	let lakersLegends = ["Kobe", "Shaq", "Lebron", "Magic", "Westbrook","Kareem"];

	console.log(lakersLegends[1]);
	console.log(lakersLegends[3]);

	// We can also save /store array elements in another variable

	let currentLaker = lakersLegends[2];
	console.log(currentLaker);

	// We can also reassign array value using items indices

	console.log("Array berfore the reassignment");
	console.log(lakersLegends);

	lakersLegends[4] = "Paul Gasol";
	console.log("Array after the reassignment");
	console.log(lakersLegends);

	// Accessing the lat element of an array
		// Since the first element of an array starts at 0, subtracting 1 to the length of an array will offset the value of one allowing us to get the element.

	console.log(lakersLegends[lakersLegends.length-1]);

	// Adding elements into array without using array methods

	let newArr = [];
	newArr[newArr.length]="Cloud Strife";
	console.log(newArr);
	newArr[newArr.length]="Tifa Lockhart";
	console.log(newArr);
	newArr.length = newArr.length-1;
	console.log(newArr);

	let emptyArr = [];
	console.log(emptyArr);
	emptyArr.length = emptyArr.length+1;
	console.log(emptyArr);
	emptyArr[emptyArr.length-1] = "Jimmy";
	console.log(emptyArr);

// [SECTION] Looping over an array
	// you can use for loop to iterate over all items in an array  

	let numberArr = [5, 12, 30, 46, 40];

	for(let index = 0; index < numberArr.length; index++){
		console.log(numberArr[index]);
	}

	for(let index = 0; index < numberArr.length; index++){
		if (numberArr[index] % 5 === 0) {
			console.log(numberArr[index]+" is divisible by 5.")
		}else{
			console.log(numberArr[index] + " is not divisible by 5.")
		}
	}

// [SECTION] Multidimensional arrays are useful for strong complex data structures.

	// a practical application of this is to help visualize/create real world objects
	// though useful in a number, creating complex array structure is now always recommended.

let chessBoard = [
	['a1', 'b1', 'c1', 'd1', 'e1', 'f1', 'g1', 'h1'],
	['a2', 'b2', 'c2', 'd2', 'e2', 'f2', 'g2', 'h2'],
	['a3', 'b3', 'c3', 'd3', 'e3', 'f3', 'g3', 'h3'],
	['a4', 'b4', 'c4', 'd4', 'e4', 'f4', 'g4', 'h4'],
	['a5', 'b5', 'c5', 'd5', 'e5', 'f5', 'g5', 'h5'],
	['a6', 'b6', 'c6', 'd6', 'e6', 'f6', 'g6', 'h6'],
	['a7', 'b7', 'c7', 'd7', 'e7', 'f7', 'g7', 'h7'],
	['a8', 'b8', 'c8', 'd8', 'e8', 'f8', 'g8', 'h8'],
	]
console.log(chessBoard);
console.table(chessBoard);
console.log(chessBoard[0][3]);
console.log(chessBoard[3]);
console.log(chessBoard[3][5]);